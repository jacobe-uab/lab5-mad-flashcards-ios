//
//  FlashCardSetDetailActivity.swift
//  CS422L
//
//  Created by Jonathan Sligh on 2/3/21.
//

import Foundation
import UIKit
import CoreData

class FlashCardSetDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    var set: FlashcardSetEntity?
    @IBOutlet var buttonView: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var deleteButton: UIButton!
    @IBOutlet var studyButton: UIButton!
    @IBOutlet var addButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        makeItPretty()
    }
    
    //adds card
    @IBAction func addCard(_ sender: Any) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "FlashcardEntity", in: managedContext)!
        let card = NSManagedObject(entity: entity, insertInto: managedContext) as! FlashcardEntity
        card.term = "Term \(set!.cards!.count + 1)"
        card.definition = "Definition \(set!.cards!.count + 1)"
        set!.addToCards(card)
        tableView.reloadData()
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func updateCard(card: FlashcardEntity, term: String, definition: String) {
        card.term = term
        card.definition = definition
        tableView.reloadData()
        do {
            try card.managedObjectContext!.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func removeCard(card: FlashcardEntity) {
        let index = set!.cards!.index(of: card)
        set!.removeFromCards(card)
        tableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: UITableView.RowAnimation.automatic)
        
        do {
            try card.managedObjectContext!.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    @IBAction func exitDetailView(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return set!.cards!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardCell", for: indexPath) as! FlashcardTableViewCell
        cell.flashcardLabel.text = (set!.cards![indexPath.row] as! FlashcardEntity).term
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let alert = UIAlertController(title: "\((set!.cards![indexPath.row] as! FlashcardEntity).term ?? "null")", message: "\((set!.cards![indexPath.row] as! FlashcardEntity).definition ?? "null")", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Edit", style: .default, handler: {_ in
            self.createCustomAlert(card: self.set!.cards![indexPath.row] as! FlashcardEntity)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    func createCustomAlert(card: FlashcardEntity)
    {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let alertVC = sb.instantiateViewController(identifier: "EditAlertViewController") as! EditAlertViewController
        alertVC.parentVC = self
        alertVC.card = card
        alertVC.modalPresentationStyle = .overCurrentContext
        self.present(alertVC, animated: false, completion: nil)
    }
    
    //just a function to make everything look nice
    func makeItPretty()
    {
        buttonView.layer.cornerRadius = 8.0
        buttonView.layer.borderColor = UIColor.purple.cgColor
        buttonView.layer.borderWidth = 2.0
        deleteButton.layer.cornerRadius = 8.0
        studyButton.layer.cornerRadius = 8.0
        addButton.layer.cornerRadius = 8.0
    }
    
    
}
